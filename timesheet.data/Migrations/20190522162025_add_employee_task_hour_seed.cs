﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace timesheet.data.Migrations
{
    public partial class add_employee_task_hour_seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                
                INSERT INTO EmployeeTaskHours values(1,1,0,3,4,0,0,0,0)
                INSERT INTO EmployeeTaskHours values(1,2,0,1,7,0,3,0,0)
                INSERT INTO EmployeeTaskHours values(2,3,0,1,2,0,7,0,0)
                INSERT INTO EmployeeTaskHours values(3,4,0,2,2,3,0,0,0)
                INSERT INTO EmployeeTaskHours values(4,5,0,3,8,0,3,0,0)
                  GO  ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           
        }
    }
}
