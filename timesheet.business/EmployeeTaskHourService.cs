﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeTaskHourService
    {
        public TimesheetDb db { get; }
        public EmployeeTaskHourService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<EmployeTaskHour> GetEmployeeTaskHours()
        {
            return this.db.EmployeeTaskHours;
        }
        public void SaveTaskHours(EmployeTaskHour[] employeeTaskHours)
        {
            this.db.EmployeeTaskHours.UpdateRange(employeeTaskHours.Where(x => x.Id != 0));
            this.db.EmployeeTaskHours.AddRange(employeeTaskHours.Where(x => x.Id == 0));
            this.db.SaveChanges();
        }
    }
}
