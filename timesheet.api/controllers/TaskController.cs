﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/v1/task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _taskService;
        public TaskController(TaskService taskService)
        {
            this._taskService = taskService;
        }

        [HttpGet]
        public IActionResult GetAll(string text)
        {
            var items = this._taskService.GetTasks().Select(x => new { Id = x.Id,Name = x.Name}).ToList();
            return new ObjectResult(items);
        }
    }
}