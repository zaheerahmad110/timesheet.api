﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService employeeService;
        public EmployeeController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet]
        public IActionResult GetAll(string text)
        {
            var items = this.employeeService.GetEmployees().Select(x => new { Id = x.Id,Name=x.Name,Code = x.Code,TaskHours = x.EmployeeTaskHours}).ToList();
            return new JsonResult(items);
        }
        [HttpGet("getddemployee")]
        public IActionResult GetEmployee(string text)
        {
            var items = this.employeeService.GetEmployees().Select(x => new { Id = x.Id,Name = x.Name });
            return new ObjectResult(items);
        }
    }
}