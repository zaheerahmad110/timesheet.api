﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/taskhours")]
    [ApiController]
    public class EmployeeTaskHourController : ControllerBase
    {
        private readonly EmployeeTaskHourService _employeeTaskHourService;
        public EmployeeTaskHourController(EmployeeTaskHourService employeeTaskHourService)
        {
            this._employeeTaskHourService = employeeTaskHourService;
        }

        [HttpGet("{EmployeeId}")]
        public IActionResult GetAll(int EmployeeId)
        {
            var items = this._employeeTaskHourService.GetEmployeeTaskHours().Where(x => x.EmployeeId == EmployeeId)
                .Select(x => new {
                    Id = x.Id,
                    TaskId = x.TaskId,
                    EmployeeId = x.EmployeeId,
                    TaskName = x.Task.Name,
                    Monday = x.Monday,
                    Tuesday = x.Tuesday,
                    Wednesday = x.Wednesday,
                    Thursday = x.Thursday,
                    Friday = x.Friday,
                    Saturday = x.Saturday,
                    Sunday = x.Sunday
                });
            return new ObjectResult(items);
        }

        [HttpPost]
        public IActionResult Save([FromBody] EmployeTaskHour[] taskHours)
        {
            this._employeeTaskHourService.SaveTaskHours(taskHours);
            return Ok(new { status = "success"});
        }
    }
}